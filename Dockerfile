FROM registry.gitlab.com/poptech/dev-ops/wordpress:4.8.1

RUN mkdir /tmp/code

ADD docker-entrypoint.sh /tmp/docker-entrypoint.sh

ADD . /tmp/code

RUN chmod +x /tmp/docker-entrypoint.sh

VOLUME ["/var/www/html"]

ENTRYPOINT ["/tmp/docker-entrypoint.sh"]

CMD ["apache2-foreground"]
